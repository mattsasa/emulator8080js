const express =  require("express")
const http =  require("http")
const app = express()
const server = http.Server(app)
const port = 3000

app.use(express.static(__dirname))
server.listen(port, () => {
  console.log(`Listening on port ${port}!!`)
})
