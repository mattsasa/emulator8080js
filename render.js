let myCanvas = document.getElementById("myCanvas")
let ctx = myCanvas.getContext("2d")
ctx.fillStyle = "#000000"

function updateView() {
  //console.log("update");

  let pixels = [], videoPointer = 9216
  for (height = 0; height < 224; height++) {
    pixels.push([])
    for (width = 0; width < 256; width+=8) {
      let bits = state.memory[videoPointer]
      videoPointer++
      for (p = 0; p < 8; p++){
        let bit = 0!= (bits & (1<<p))
        bit ? bit = pixels[height].push('#') : bit = pixels[height].push(' ')
      }
    }
  }

  let y = 0
  for (w = 255; w > -1; w--) {
    y+=2; let x = 0
    for (h = 0; h < 224; h++) {
      x+=2
      if (pixels[h][w] == ' ') ctx.fillStyle = "#000000"
      else ctx.fillStyle = "#FFFFFF"
      ctx.fillRect(x, y, 2, 2)
    }
  }
}

const cyclesByOpcode = [
  4, 10, 7, 6, 5, 5, 7, 7, 4, 11, 7, 6, 5, 5, 7, 4, //0x00..0x0f
	4, 10, 7, 6, 5, 5, 7, 7, 4, 11, 7, 6, 5, 5, 7, 4, //0x10..0x1f
	4, 10, 16, 6, 5, 5, 7, 4, 4, 11, 16, 6, 5, 5, 7, 4, //etc
	4, 10, 13, 6, 10, 10, 10, 4, 4, 11, 13, 6, 5, 5, 7, 4,

	5, 5, 5, 5, 5, 5, 7, 5, 5, 5, 5, 5, 5, 5, 7, 5, //0x40..0x4f
	5, 5, 5, 5, 5, 5, 7, 5, 5, 5, 5, 5, 5, 5, 7, 5,
	5, 5, 5, 5, 5, 5, 7, 5, 5, 5, 5, 5, 5, 5, 7, 5,
	7, 7, 7, 7, 7, 7, 7, 7, 5, 5, 5, 5, 5, 5, 7, 5,

	4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4, //0x80..8x4f
	4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
	4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
	4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,

	11, 10, 10, 10, 18, 11, 7, 11, 11, 10, 10, 10, 11, 17, 7, 11, //0xc0..0xcf
  11, 10, 10, 10, 18, 11, 7, 11, 11, 10, 10, 10, 11, 17, 7, 11, //0xd0..0xdf
	11, 10, 10, 4, 18, 11, 7, 11, 11, 4, 10, 4, 18, 0, 7, 11,
  11, 10, 10, 4, 18, 11, 7, 11, 11, 6, 10, 4, 18, 0, 7, 11
]

let state = {
  cycles: 0,
  steps: 0,
  a: 0,
  b: 0,
  c: 0,
  d: 0,
  e: 0,
  h: 0,
  l: 0,
  sp: 0,
  pc: 0,
  memory: [],
  flags: {
    z: false,
    s: false,
    p: false,
    cy: false,
    ac: false,
    pad: 3
  },
  input: {
    coin: false,
    player1: false,
    fire: false,
    left: false,
    right: false
  },
  int_enable: false,
  interruptMode: false,
  lastInterrupt: 8
}

function hexToDec(hexChar) { return parseInt(hexChar, 16) }

function seperateBytes(word) {
  let ws = word.toString(16); let l = ws.length
  if (l == 4) return { first: (hexToDec(ws[0]) * 16) + hexToDec(ws[1]), second: (hexToDec(ws[2]) * 16) + hexToDec(ws[3]) }
  if (l == 3) return { first: hexToDec(ws[0]), second: (hexToDec(ws[1]) * 16) + hexToDec(ws[2]) }
  if (l == 2) return { first: 0, second: (hexToDec(ws[0]) * 16) + hexToDec(ws[1]) }
  if (l == 1) return { first: 0, second: hexToDec(ws[0]) }
}

function convertInputToByte() {
  let byte = 0
  if (state.input.coin) byte += 1
  if (state.input.player1) byte += 4
  if (state.input.fire) byte += 16
  if (state.input.left) byte += 32
  if (state.input.right) byte += 64
  return byte
}

function convertFlagsToByte() {
  let byte = 0
  if (state.flags.z) byte += 128
  if (state.flags.s) byte += 64
  if (state.flags.p) byte += 32
  if (state.flags.cy) byte += 16
  return byte
}

function restoreFlagsFromByte(byte) {
  if (byte > 127) { state.flags.z = true; byte -= 128 }
  else { state.flags.z = false }
  if (byte > 63) { state.flags.s = true; byte -= 64 }
  else { state.flags.s = false }
  if (byte > 31) { state.flags.p = true; byte -= 32 }
  else { state.flags.p = false }
  if (byte > 15) { state.flags.cy = true; }
  else { state.flags.cy = false }
}

function setZSPflags(value) {
  state.flags.z = value == 0
  state.flags.s = value > 127 || value < 0
  state.flags.p = value % 2 == 0
}

function unimplementedOpcode (opcode, arg1, arg2) {
  console.log("Error: unimplemented Opcode")
  console.log("Stopped before opcode: " + opcode.toString(16) + "  args: " + arg1 + " " + arg2 + " Steps: " + state.steps)
}

function emulate() {
  const opcode = state.memory[state.pc]
  const arg1 = state.memory[state.pc + 1]
  const arg2 = state.memory[state.pc + 2]
  state.pc++

  switch (opcode) {
    case 0x00: break //NOP
    case 0x01:
      state.c = arg1; state.b = arg2; state.pc += 2; break
    case 0x02:
      addr = (state.b * 256) + state.c; state.memory[addr] = state.a; break
    case 0x03:
      bc = (state.b * 256) + state.c; bc += 1; if (bc == 65535) bc = 0
      bytes = seperateBytes(bc); state.b = bytes.first; state.c = bytes.second; break
    case 0x04:
      state.b++; if (state.b == 256) state.b = 0; setZSPflags(state.b); break
    case 0x05:
      state.b--; if (state.b == -1) state.b = 255; setZSPflags(state.b); break
    case 0x06:
      state.b = arg1; state.pc++; break
    case 0x07: //RLC
      temp_a = state.a * 2; if (state.a > 127) temp_a++; state.flags.cy = (state.a > 127);
      state.a = temp_a; break
    case 0x08: break //NOP
    case 0x09:
      hl = (state.h * 256) + state.l;  bc = (state.b * 256) + state.c;
      hl += bc; state.flags.cy = hl > 65535; if (hl > 65535) hl -= 65536
      bytes = seperateBytes(hl); state.h = bytes.first; state.l = bytes.second; break
    case 0x0a:
      addr = (state.b * 256) + state.c; state.a = state.memory[addr]; break
    case 0x0c:
      state.c++; if (state.c == 256) state.c = 0; setZSPflags(state.c); break
    case 0x0d:
      state.c--; if (state.c == -1) state.c = 255; setZSPflags(state.c); break
    case 0x0e:
      state.c = arg1; state.pc++; break
    case 0x0f:
      temp = state.a; temp = Math.floor(temp / 2); if (state.a % 2 != 0) temp += 128;
      state.flags.cy = (state.a % 2 != 0); state.a = temp; break
    case 0x10: break //NOP
    case 0x11:
      state.e = arg1; state.d = arg2; state.pc += 2; break
    case 0x12:
      addr = (state.d * 256) + state.e; state.memory[addr] = state.a; break
    case 0x13:
      state.e++; if (state.e == 256) { state.e = 0; state.d++ } break
    case 0x14:
      state.d++; if (state.d == 256) state.d = 0; setZSPflags(state.d); break
    case 0x15:
      state.d--; if (state.d == -1) state.d = 255; setZSPflags(state.d); break
    case 0x16:
      state.d = arg1; state.pc++; break
    case 0x19:
      hl = (state.h * 256) + state.l;  let de = (state.d * 256) + state.e;
      hl += de; state.flags.cy = hl > 65535; if (hl > 65535) hl -= 65536
      bytes = seperateBytes(hl); state.h = bytes.first; state.l = bytes.second;
      break
    case 0x1a:
      addr = (state.d * 256) + state.e; state.a = state.memory[addr]; break
    case 0x1b:
      addr = (state.d * 256) + state.e; addr--; if (addr == -1) addr = 65535
      bytes = seperateBytes(addr); state.d = bytes.first; state.e = bytes.second; break
    case 0x1f:
      temp = state.a; temp = Math.floor(temp / 2); if (state.flags.cy) temp += 128;
      state.flags.cy = (state.a % 2 != 0); state.a = temp; break
    case 0x20: break //NOP
    case 0x21:
      state.h = arg2; state.l = arg1; state.pc += 2; break
    case 0x22:
      addr = (arg2 * 256) + arg1; state.memory[addr] = state.l; state.memory[addr+1] = state.h
      state.pc += 2; break
    case 0x23:
      state.l++; if (state.l == 256) { state.l = 0; state.h++ } break
    case 0x26:
      state.h = arg1; state.pc++; break
    case 0x27: break //Special
    case 0x29:
      hl = (state.h * 256) + state.l; hl += hl; state.flags.cy = hl > 65535; if (hl > 65535) hl -= 65536
      bytes = seperateBytes(hl); state.h = bytes.first; state.l = bytes.second; break
    case 0x2a:
      addr = (arg2 * 256) + arg1; state.l = state.memory[addr]; state.h = state.memory[addr + 1]; state.pc += 2; break
    case 0x2b:
      hl = (state.h * 256) + state.l; hl--; if (hl == -1) hl = 65535;
      bytes = seperateBytes(hl); state.h = bytes.first; state.l = bytes.second; break
    case 0x2c:
      state.l++; if (state.l == 256) state.l = 0; setZSPflags(state.l); break
    case 0x2e:
      state.l = arg1; state.pc++; break
    case 0x2f:
      state.a = ~state.a; break
    case 0x31:
      state.sp = (arg2 * 256) + arg1; state.pc += 2; break
    case 0x32:
      addr = (arg2 * 256) + arg1; state.memory[addr] = state.a; state.pc += 2; break
    case 0x34:
      addr = (state.h * 256) + state.l; value = state.memory[addr]; value += 1;
      if (value > 255) value = 0; setZSPflags(value); state.memory[addr] = value; break
    case 0x35:
      addr = (state.h * 256) + state.l; value = state.memory[addr]; value -= 1;
      if (value < 0) value = 255; setZSPflags(value); state.memory[addr] = value; break
    case 0x36:
      addr = (state.h * 256) + state.l; state.memory[addr] = arg1; state.pc++; break
    case 0x37:
      state.flags.cy = true; break
    case 0x3a:
      addr = (arg2 * 256) + arg1; state.a = state.memory[addr]; state.pc += 2; break
    case 0x3c:
      state.a++; if (state.a == 256) state.a = 0; setZSPflags(state.a); break
    case 0x3d:
      state.a--; if (state.a == -1) state.a = 255; setZSPflags(state.a); break
    case 0x3e:
      state.a = arg1; state.pc++; break
    case 0x40: break //NOP
    case 0x41:
      state.b = state.c; break
    case 0x45:
      state.b = state.l; break
    case 0x46:
      addr = (state.h * 256) + state.l; state.b = state.memory[addr]; break
    case 0x47:
      state.b = state.a; break
    case 0x48:
      state.c = state.b; break
    case 0x49: // MOV C,C
      break
    case 0x4a:
      state.c = state.d; break
    case 0x4c:
      state.c = state.h; break
    case 0x4e:
      addr = (state.h * 256) + state.l; state.c = state.memory[addr]; break
    case 0x4f:
      state.c = state.a; break
    case 0x51:
      state.d = state.c; break
    case 0x56:
      addr = (state.h * 256) + state.l; state.d = state.memory[addr]; break
    case 0x57:
      state.d = state.a; break
    case 0x5e:
      addr = (state.h * 256) + state.l; state.e = state.memory[addr]; break
    case 0x5f:
      state.e = state.a; break
    case 0x61:
      state.h = state.c; break
    case 0x62:
      state.h = state.d; break
    case 0x63:
      state.h = state.e; break
    case 0x64: break  // MOV H<-H
    case 0x65:
      state.h = state.l; break
    case 0x66:
      addr = (state.h * 256) + state.l; state.h = state.memory[addr]; break
    case 0x67:
      state.h = state.a; break
    case 0x68:
      state.l = state.b; break
    case 0x69:
      state.l = state.c; break
    case 0x6a:
      state.l = state.d; break
    case 0x6b:
      state.l = state.e; break
    case 0x6c:
      state.l = state.h; break
    case 0x6f:
      state.l = state.a; break
    case 0x70:
      addr = (state.h * 256) + state.l; state.memory[addr] = state.b; break
    case 0x71:
      addr = (state.h * 256) + state.l; state.memory[addr] = state.c; break
    case 0x72:
      addr = (state.h * 256) + state.l; state.memory[addr] = state.d; break
    case 0x73:
      addr = (state.h * 256) + state.l; state.memory[addr] = state.e; break
    case 0x77:
      addr = (state.h * 256) + state.l; state.memory[addr] = state.a; break
    case 0x78:
      state.a = state.b; break
    case 0x79:
      state.a = state.c; break
    case 0x7a:
      state.a = state.d; break
    case 0x7b:
      state.a = state.e; break
    case 0x7c:
      state.a = state.h; break
    case 0x7d:
      state.a = state.l; break
    case 0x7e:
      addr = (state.h * 256) + state.l; state.a = state.memory[addr]; break
    case 0x7f: // MOV A,A
      break
    case 0x80:
      state.a += state.b; setZSPflags(state.a)
      state.flags.cy = (state.a > 255); if (state.a > 255) state.a -= 256; break
    case 0x81:
      state.a += state.c; setZSPflags(state.a)
      state.flags.cy = (state.a > 255); if (state.a > 255) state.a -= 256; break
    case 0x83:
      state.a += state.e; setZSPflags(state.a)
      state.flags.cy = (state.a > 255); if (state.a > 255) state.a -= 256; break
    case 0x85:
      state.a += state.l; setZSPflags(state.a)
      state.flags.cy = (state.a > 255); if (state.a > 255) state.a -= 256; break
    case 0x86:
      addr = (state.h * 256) + state.l; state.a += state.memory[addr]; setZSPflags(state.a)
      state.flags.cy = (state.a > 255); if (state.a > 255) state.a -= 256; break
    case 0x8a:
      state.a += state.d; if (state.flags.cy) state.a++; setZSPflags(state.a)
      state.flags.cy = (state.a > 255); if (state.a > 255) state.a -= 256; break
    case 0x97:
      state.a = 0; setZSPflags(0); state.flags.cy = false; break
    case 0xa0:
      state.a = state.a & state.b; setZSPflags(state.a); state.flags.cy = false; break
    case 0xa1:
      state.a = state.a & state.c; setZSPflags(state.a); state.flags.cy = false; break
    case 0xa6:
      addr = (state.h * 256) + state.l; value = state.memory[addr]
      state.a = state.a & value; setZSPflags(state.a); state.flags.cy = false; break
    case 0xa7:
      state.a = state.a & state.a; setZSPflags(state.a); state.flags.cy = false; break
    case 0xa8:
      state.a = state.a ^ state.b; setZSPflags(state.a); state.flags.cy = false; break
    case 0xaf:
      state.a = 0; setZSPflags(0); state.flags.cy = false; break
    case 0xb0:
      state.a = state.a | state.b; setZSPflags(state.a); state.flags.cy = false; break
    case 0xb1:
      state.a = state.a | state.c; setZSPflags(state.a); state.flags.cy = false; break
    case 0xb2:
      state.a = state.a | state.d; setZSPflags(state.a); state.flags.cy = false; break
    case 0xb3:
      state.a = state.a | state.e; setZSPflags(state.a); state.flags.cy = false; break
    case 0xb4:
      state.a = state.a | state.h; setZSPflags(state.a); state.flags.cy = false; break
    case 0xb5:
      state.a = state.a | state.l; setZSPflags(state.a); state.flags.cy = false; break
    case 0xb6:
      addr = (state.h * 256) + state.l; state.a = state.a | state.memory[addr];
      setZSPflags(state.a); state.flags.cy = false; break
    case 0xb8:
      value = state.a - state.b; state.flags.cy = (value < 0); setZSPflags(value); break
    case 0xbc:
      value = state.a - state.h; state.flags.cy = (value < 0); setZSPflags(value); break
    case 0xbe:
      addr = (state.h * 256) + state.l;
      value = state.a - state.memory[addr]; state.flags.cy = (value < 0); setZSPflags(value); break
    case 0xc0:
      if (!state.flags.z) { state.pc = (state.memory[state.sp + 1] * 256) + state.memory[state.sp]; state.sp += 2 } break
    case 0xc1:
      state.b = state.memory[state.sp + 1]; state.c = state.memory[state.sp]; state.sp += 2; break
    case 0xc2:
      !state.flags.z ? state.pc = (arg2 * 256) + arg1 : state.pc += 2; break
    case 0xc3: //Jump
      state.pc = (arg2 * 256) + arg1; break
    case 0xc4:
      if (!state.flags.z) {
        state.memory[state.sp - 1] = seperateBytes(state.pc+2).first
        state.memory[state.sp - 2] = seperateBytes(state.pc+2).second
        state.sp -= 2; state.pc = (arg2 * 256) + arg1;
      } else { state.pc += 2 }
      break
    case 0xc5:
      state.memory[state.sp - 2] = state.c; state.memory[state.sp - 1] = state.b; state.sp -= 2; break
    case 0xc6:
      state.a += arg1; setZSPflags(state.a); state.flags.cy = (state.a > 255)
      if (state.a > 255) state.a -= 256; state.pc++; break
    case 0xc8:
      if (state.flags.z) { state.pc = (state.memory[state.sp + 1] * 256) + state.memory[state.sp]; state.sp += 2 }
      break
    case 0xc9:
      state.pc = (state.memory[state.sp + 1] * 256) + state.memory[state.sp]; state.sp += 2; break
    case 0xca:
      if (state.flags.z) { addr = (arg2 * 256) + arg1; state.pc = addr; break }
      else { state.pc += 2; break }
    case 0xcc:
      if (state.flags.z) {
        state.memory[state.sp - 1] = seperateBytes(state.pc+2).first
        state.memory[state.sp - 2] = seperateBytes(state.pc+2).second
        state.sp -= 2; state.pc = (arg2 * 256) + arg1;
      } else { state.pc += 2 } break
    case 0xcd: //Call
      state.memory[state.sp - 1] = seperateBytes(state.pc+2).first
      state.memory[state.sp - 2] = seperateBytes(state.pc+2).second
      state.sp -= 2; state.pc = (arg2 * 256) + arg1; break
    case 0xd0:
      if (!state.flags.cy) { state.pc = (state.memory[state.sp + 1] * 256) + state.memory[state.sp]; state.sp += 2 } break
    case 0xd1:
      state.d = state.memory[state.sp + 1]; state.e = state.memory[state.sp]; state.sp += 2; break
    case 0xd2:
      if (!state.flags.cy) { addr = (arg2 * 256) + arg1; state.pc = addr; break }
      else { state.pc += 2; break }
    case 0xd3: //Special
      state.pc++; break
    case 0xd4:
      if (!state.flags.cy) {
        state.memory[state.sp - 1] = seperateBytes(state.pc+2).first
        state.memory[state.sp - 2] = seperateBytes(state.pc+2).second
        state.sp -= 2; state.pc = (arg2 * 256) + arg1; break
      } else { state.pc += 2 } break
    case 0xd5:
      state.memory[state.sp - 2] = state.e; state.memory[state.sp - 1] = state.d; state.sp -= 2; break
    case 0xd6:
      state.a -= arg1; setZSPflags(state.a); state.flags.cy = (state.a < 0)
      if (state.a < 0) state.a += 256; state.pc++; break
    case 0xd8:
      if (state.flags.cy) { state.pc = (state.memory[state.sp + 1] * 256) + state.memory[state.sp]; state.sp += 2 }
      break
    case 0xda: //JC
      if (state.flags.cy) { addr = (arg2 * 256) + arg1; state.pc = addr }
      else { state.pc += 2 } break
    case 0xdc:
      if (state.flags.cy) {
        state.memory[state.sp - 1] = seperateBytes(state.pc+2).first
        state.memory[state.sp - 2] = seperateBytes(state.pc+2).second
        state.sp -= 2; state.pc = (arg2 * 256) + arg1; break
      } else { state.pc += 2 } break
    case 0xdb: //Special
      state.pc++; if(arg1 != 1) return; state.a = convertInputToByte(); break
    case 0xde:
      state.a -= arg1; if (state.flags.cy) state.a--; setZSPflags(state.a); state.flags.cy = (state.a < 0)
      if (state.a < 0) state.a += 256; state.pc++; break
    case 0xe1:
      state.h = state.memory[state.sp + 1]; state.l = state.memory[state.sp]; state.sp += 2; break
    case 0xe3:
      l_temp = state.l; h_temp = state.h; state.l = state.memory[state.sp]; state.h = state.memory[state.sp + 1]
      state.memory[state.sp] = l_temp; state.memory[state.sp + 1] = h_temp;
      break
    case 0xe5:
      state.memory[state.sp - 2] = state.l; state.memory[state.sp - 1] = state.h; state.sp -= 2; break
    case 0xe6:
      state.a = state.a & arg1; state.pc++; state.flags.cy = false; setZSPflags(state.a); break
    case 0xe9:
      state.pc = (state.h * 256) + state.l; break
    case 0xeb:
      d = state.d; e = state.e; state.d = state.h; state.e = state.l; state.h = d; state.l = e; break
    case 0xf1:
      restoreFlagsFromByte(state.memory[state.sp]); state.a = state.memory[state.sp + 1]; state.sp += 2; break
    case 0xf5:
      state.memory[state.sp - 2] = convertFlagsToByte(); state.memory[state.sp - 1] = state.a; state.sp -= 2; break
    case 0xf6:
      state.flags.cy = false; state.a = state.a | arg1; setZSPflags(state.a); state.pc++; break
    case 0xfa:
      addr = (arg2 * 256) + arg1; if (state.flags.s) state.pc = addr; else state.pc += 2; break
    case 0xfb: //Special
      state.int_enable = true; break
    case 0xfe:
      value = state.a - arg1; state.flags.cy = (value < 0); setZSPflags(value); state.pc++; break
    default:
      unimplementedOpcode(opcode, arg1, arg2)
  }
  state.cycles += cyclesByOpcodeCpp[opcode]
  if (state.cycles >= 16667) {
    state.cycles -= 16667
    if (!state.int_enable) return
    state.interruptMode = true
    state.lastInterrupt == 16 ? state.lastInterrupt = 8 : state.lastInterrupt = 16
    state.memory[state.sp - 1] = seperateBytes(state.pc).first
    state.memory[state.sp - 2] = seperateBytes(state.pc).second
    state.sp -= 2; state.pc = state.lastInterrupt
  }
}

function step() {
  if (!state.interruptMode) state.steps++
  if (state.pc == 135) state.interruptMode = false
  emulate()
  //if (state.steps % 10000 == 0) updateView()
}

function run() {
  const mainInterval = setInterval(function() { for(var i = 0; i < 20000; ++i) { step() } }, 10)
  const drawInterval = setInterval(function() { updateView() }, 15)
}

var xhr = new XMLHttpRequest()
xhr.open('GET', '/invaders.rom', true)
xhr.responseType = 'arraybuffer'
xhr.onload = function(e) {
  if (this.status == 200) {
    var arrayBuffer = this.response;
    var byteArray = new Uint8Array(arrayBuffer);
    for (var i = 0; i < 96384; i++) {
      if (byteArray[i]) state.memory[i] = byteArray[i]
      else state.memory[i] = 0
    }
  }
  run()
}
xhr.send()

document.addEventListener("keydown", function(e) {
     var evt = e || window.event
     if(evt.keyCode == 67) { state.input.coin = true }
     if(evt.keyCode == 49) { state.input.player1 = true }
     if(evt.keyCode == 32) { state.input.fire = true}
     if(evt.keyCode == 37) { state.input.left = true }
     if(evt.keyCode == 39) { state.input.right = true }
     return false
   }, false)

 document.addEventListener("keyup", function(e) {
      var evt = e || window.event
      if(evt.keyCode == 67) { state.input.coin = false }
      if(evt.keyCode == 49) { state.input.player1 = false }
      if(evt.keyCode == 32) { state.input.fire = false}
      if(evt.keyCode == 37) { state.input.left = false }
      if(evt.keyCode == 39) { state.input.right = false }
      return false
    }, false)
