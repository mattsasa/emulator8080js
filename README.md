# emulatorJS

Attempt to build an 8080 emulator to play space invaders in javascript.

The project started with intent to do the emulation server side in NodeJS and stream the pixel data to a light client. 
However, was unable to get desired performance easily, so switched to running the emulation client side. Now, the server only serves the client side JS and serves the Space Invaders ROM. 
There is an alternate branch that still has the server side emulation code.